import { Component } from '@angular/core';
import { Role } from 'src/app/enums/role';
import { TokenService } from 'src/app/services/token.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
	role = Role;

	constructor(public tokenService: TokenService) {}
}
