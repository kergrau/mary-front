import { CanActivateFn, Router } from '@angular/router';
import { TokenService } from '../services/token.service';
import { inject } from '@angular/core';
import { Role } from 'src/app/enums/role';

export const adminGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  const tokenService: TokenService = inject(TokenService);
  if (tokenService.hasRole(Role.ADMIN))
    return true;

  router.navigate(["/dashboard"]);
  return false;
};
