import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { UserForm } from 'src/app/interfaces/forms/user-form';
import { RegisterRequest } from 'src/app/interfaces/register-request';
import { GenericDropdown } from 'src/app/interfaces/generic-dropdown';
import { UploadFileService } from 'src/app/services/upload-file.service';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss'],
    providers: [MessageService]
})
export class SignUpComponent implements OnInit {
    declare form: FormGroup<UserForm>;
    registerRequest!: RegisterRequest;
    listIdentificationType: GenericDropdown[] = [
        {name: "Cedula de ciudadania", value: "CC"},
        {name: "Cedula de extranjeria", value: "CE"},
        {name: "Pasaporte ", value: "PP"}
    ];

    listBankAccountType: GenericDropdown[] = [
        {name: "Cuenta de Ahorros", value: "CA"},
        {name: "Cuenta Corriente", value: "CC"},
        {name: "Billetera virtual ", value: "BV"}
    ];
    
    fileRut: File | undefined;
    fileDni: File | undefined;
    fileBankRef: File | undefined;
    fileSign: File | undefined;
    uploadRut: boolean = false;
    uploadDni: boolean = false;
    uploadBankRef: boolean = false;
    uploadSign: boolean = false;
    loading: boolean = false;

    constructor(
        private fb: FormBuilder, 
        private authService: AuthService, 
        private uploadFileService: UploadFileService,
        private messageService: MessageService,
        private router: Router
        ) {}

    ngOnInit(): void {
        this.initSignUpForm();
    }

    initSignUpForm(): void {
        this.form = this.fb.nonNullable.group({
            identificationType: ['', [Validators.required]],
            dni: [
                '',
                [
                    Validators.required,
                    Validators.minLength(6),
                    Validators.maxLength(10),
                ],
            ],
            name1: [
                '',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(40),
                    Validators.pattern(/^[a-zA-Z]+$/),
                ],
            ],
            name2: [
                '',
                [
                    Validators.minLength(2),
                    Validators.maxLength(40),
                    Validators.pattern(/^[a-zA-Z]+$/),
                ],
            ],
            surname1: [
                '',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(40),
                    Validators.pattern(/^[a-zA-Z]+$/),
                ],
            ],
            surname2: [
                '',
                [
                    Validators.minLength(2),
                    Validators.maxLength(40),
                    Validators.pattern(/^[a-zA-Z]+$/),
                ],
            ],
            email: [
                '',
                [
                    Validators.required,
                    Validators.email,
                    Validators.maxLength(50),
                ],
            ],
            address: ['', Validators.maxLength(100)],
            city: ['', Validators.maxLength(30)],
            phone: [
                '',
                [Validators.maxLength(10),
                Validators.pattern(/^[0-9]+$/),]
            ],
            username: [
                '',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(50),
                    Validators.pattern(/^[a-zA-Z0-9]+$/),
                ],
            ],
            password: [
                '',
                [
                    Validators.required,
                    Validators.minLength(6),
                    Validators.maxLength(16),
                ],
            ],
            confirmPassword: ['', Validators.required],

            userFinancialInfo: this.fb.nonNullable.group({
                rut: ['', [Validators.required]],
                dniSupport: ['', [Validators.required]],
                bankReference: ['', [Validators.required]],
                bankAccountType: ['', [Validators.required]],
                bankAccount: ['', [Validators.required]],
                sign: ['', [Validators.required]],
                bankName: ['', [Validators.required]]
            }),
        }, {
            validators: this.matchPassword("password", "confirmPassword"),
        });
    }

    matchPassword(controlName: string, matchingControlName: string): ValidatorFn {
        return (abstractControl: AbstractControl) => {
            const control = abstractControl.get(controlName);
            const matchingControl = abstractControl.get(matchingControlName);
    
            if (matchingControl!.errors && !matchingControl!.errors?.['matchPassword']) {
                return null;
            }
    
            if (control!.value !== matchingControl!.value) {
              const error = { matchPassword: true };
              matchingControl!.setErrors(error);
              return error;
            } else {
              matchingControl!.setErrors(null);
              return null;
            }
        }
    }

    get f() {
        return this.form.controls;
    }

    get infoBank() {
        return this.form.controls.userFinancialInfo.controls;
    }

    selectedFile(e: any, type: string) {
        if (!this.uploadFileService.validateFileSize(e.target.files[0]))
            return this.messageService.add({ severity: 'error', summary: 'File Error', detail: 'The file must not exceed 4MB' });

        if (!this.uploadFileService.validateFileType(e.target.files[0]))
            return this.messageService.add({ severity: 'error', summary: 'File Error', detail: 'The file must be of type pdf, jpg, jpeg, png' });

        switch(type) {
            case "rut":
                this.fileRut = e.target.files[0];
                break;
            case "dni":
                this.fileDni = e.target.files[0];
                break;
            case "bankref":
                this.fileBankRef = e.target.files[0];
                break;
            case "sign":
                this.fileSign = e.target.files[0];
                break;
        }
    }

    uploadFile(type: string) {
        switch(type) {
            case "rut":
                if(this.fileRut) {
                    this.uploadFileService.upFilePinata(this.fileRut, {})
                    .then(result => {
                        this.form.controls.userFinancialInfo.controls["rut"].setValue(result);
                        this.uploadRut = !this.uploadRut;
                        this.messageService.add({ severity: 'success', summary: 'Upload File', detail: 'File uploaded successfully' });
                    }).catch(() => {
                        this.messageService.add({ severity: 'error', summary: 'Upload File Error', detail: 'Error uploading file try again' });
                    });
                } else {
                    this.messageService.add({ severity: 'error', summary: 'File Empty', detail: 'File has not been uploaded' });
                }
                break;
            case "dni":
                if(this.fileDni) {
                    this.uploadFileService.upFilePinata(this.fileDni, {})
                    .then(result => {
                        this.form.controls.userFinancialInfo.controls["dniSupport"].setValue(result);
                        this.uploadDni = !this.uploadDni;
                        this.messageService.add({ severity: 'success', summary: 'Upload File', detail: 'File uploaded successfully' });
                    }).catch(() => {
                        this.messageService.add({ severity: 'error', summary: 'Upload File Error', detail: 'Error uploading file try again' });
                    });
                } else {
                    this.messageService.add({ severity: 'error', summary: 'File Empty', detail: 'File has not been uploaded' });
                }
                break;
            case "bankref":
                if(this.fileBankRef) {
                    this.uploadFileService.upFilePinata(this.fileBankRef, {})
                    .then(result => {
                        this.form.controls.userFinancialInfo.controls["bankReference"].setValue(result);
                        this.uploadBankRef = !this.uploadBankRef;
                        this.messageService.add({ severity: 'success', summary: 'Upload File', detail: 'File uploaded successfully' });
                    }).catch(() => {
                        this.messageService.add({ severity: 'error', summary: 'Upload File Error', detail: 'Error uploading file try again' });
                    });
                } else {
                    this.messageService.add({ severity: 'error', summary: 'File Empty', detail: 'File has not been uploaded' });
                }
                break;
            case "sign":
                if(this.fileSign) {
                    this.uploadFileService.upFilePinata(this.fileSign, {})
                    .then(result => {
                        this.form.controls.userFinancialInfo.controls["sign"].setValue(result);
                        this.uploadSign = !this.uploadSign;
                        this.messageService.add({ severity: 'success', summary: 'Upload File', detail: 'File uploaded successfully' });
                    }).catch(() => {
                        this.messageService.add({ severity: 'error', summary: 'Upload File Error', detail: 'Error uploading file try again' });
                    });
                } else {
                    this.messageService.add({ severity: 'error', summary: 'File Empty', detail: 'File has not been uploaded' });
                }
                break;
        }
    }

    onSubmit(): void {
        this.registerRequest = this.form.getRawValue();
        this.loading = true;
        this.authService.signUp(this.registerRequest).subscribe({
            next: () => {
                this.messageService.add({ severity: 'success', summary: 'Success', detail: 'New user created successfully' });
                this.router.navigate(['/login']);
            },
            error: (err) => console.error("Error ->", err)
        });
    }
}
