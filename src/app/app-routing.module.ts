import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { authGuard } from './guards/auth.guard';
import { adminGuard } from './guards/admin.guard';
import { UpfileTestComponent } from './upfile-test/upfile-test.component';
import { GenerateInvoicesComponent } from './generate-invoices/generate-invoices.component';
import { PendingInvoicesComponent } from './pending-invoices/pending-invoices.component';
import { ApprovedInvoicesComponent } from './approved-invoices/approved-invoices.component';

const routes: Routes = [
	{ path: '', pathMatch: 'full', redirectTo: '/login' },
	{ path: 'login', component: LoginComponent },
	{ path: 'signup', component: SignUpComponent },
	{ path: 'test', component: UpfileTestComponent },
	{
		path: 'dashboard',
		component: HomeComponent,
		canActivate: [authGuard],
		children: [
			{ path: '', component: DashboardComponent },
			{ path: 'generate-invoices', component: GenerateInvoicesComponent },
			{
				path: 'pending-invoices',
				component: PendingInvoicesComponent,
				canActivate: [adminGuard]
			},
			{ path: 'approved-invoices', component: ApprovedInvoicesComponent }
		]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
