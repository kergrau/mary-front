import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InvoiceDetail } from 'src/app/interfaces/invoice-detail';
import { InvoiceService } from 'src/app/services/invoice.service';
import { MessageService, ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-generate-invoices',
  templateUrl: './generate-invoices.component.html',
  styleUrls: ['./generate-invoices.component.scss'],
  providers: [MessageService, ConfirmationService]
})
export class GenerateInvoicesComponent {

  loading: boolean = false;
  listInvoicesDetail: InvoiceDetail[] = []; 

  form: FormGroup = this.fb.group({
    item: ['', [Validators.required]],
    price: ['', [Validators.required]]
  });

  constructor(
    private fb: FormBuilder, 
    private invoiceService: InvoiceService, 
    private messageService: MessageService,
    private confirmationService: ConfirmationService
    ) {}

  get f() {
    return this.form.controls;
  }

  addInvoiceDetail() {
    this.listInvoicesDetail.push({item: this.form.controls['item'].value, price: this.form.controls['price'].value});
    this.form.reset();
  }

  removeInvoice(index: number) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this article?',
      header: 'Delete Confirmation',
      rejectLabel: 'Cancel',
      rejectIcon: ' ',
      rejectButtonStyleClass: 'p-button-sm p-button-outlined',
      acceptLabel: 'Delete',
      acceptIcon: ' ',
      acceptButtonStyleClass: 'p-button-sm p-button-danger',
      accept: () => {
        this.listInvoicesDetail.splice(index,1);
        this.messageService.add({ severity: 'info', summary: 'Deleted', detail: 'Article deleted' });
      }
    });
  }

  saveInvoicesDetails() {
    this.loading = true;
    this.invoiceService.save(this.listInvoicesDetail).subscribe({
      next: () => {
        this.listInvoicesDetail = [];
        this.messageService.add({ severity: 'success', summary: 'Saved', detail: 'Articles saved successfully' });
        this.loading = false;
      },
      error: (err) => console.error("Error ->",err)
    });
  }
}
