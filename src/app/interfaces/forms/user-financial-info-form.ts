import { FormControl } from '@angular/forms';

export interface UserFinancialInfoForm {
    rut: FormControl<string>;
    dniSupport: FormControl<string>;
    bankReference: FormControl<string>;
    bankAccountType: FormControl<string>;
    bankAccount: FormControl<string>;
    sign: FormControl<string>;
    bankName: FormControl<string>;
}
