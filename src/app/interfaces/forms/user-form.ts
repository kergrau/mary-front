import { FormControl, FormGroup } from '@angular/forms';
import { UserFinancialInfoForm } from './user-financial-info-form';

export interface UserForm {
    identificationType: FormControl<string>;
    dni: FormControl<string>;
    name1: FormControl<string>;
    name2: FormControl<string>;
    surname1: FormControl<string>;
    surname2: FormControl<string>;
    email: FormControl<string>;
    address: FormControl<string>;
    phone: FormControl<string>;
    city: FormControl<string>;
    username: FormControl<string>;
    password: FormControl<string>;
    confirmPassword: FormControl<string>;
    userFinancialInfo: FormGroup<UserFinancialInfoForm>;
}
