import { InvoiceDetail } from './invoice-detail';

export interface MetadataNFT {
	rut: string;
	date: Date;
	approvedBy: string;
	invoice: InvoiceDetail[];
}
