export interface InvoiceDetail {
	item: string;
	price: string;
}
