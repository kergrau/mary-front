import { InvoiceDetail } from './invoice-detail';

export interface InvoiceToApproval {
	id: number;
	generatedBy: string;
	invoiceDetails: InvoiceDetail[];
	createDate: any;
	rut: string;
}
