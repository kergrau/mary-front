import { JsonMetadataPinata } from './json-metadata-pinata';
import { MetadataNFT } from './metadata-nft';

export interface UpJsonPinataRequest {
	pinataContent: MetadataNFT;
	pinataMetadata: JsonMetadataPinata;
}
