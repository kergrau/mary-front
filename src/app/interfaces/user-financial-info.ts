export interface UserFinancialInfo {
  rut: string;
  dniSupport: string;
  bankReference: string;
  bankAccountType: string;
  bankAccount: string;
  sign: string;
  bankName: string;
}
