import { UserFinancialInfo } from "./user-financial-info";

export interface RegisterRequest {
  identificationType: string;
  dni: string;
  name1: string;
  name2: string;
  surname1: string;
  surname2: string;
  email: string;
  address: string;
  phone: string;
  city: string;
  username: string;
  password: string;
  userFinancialInfo: UserFinancialInfo;
}
