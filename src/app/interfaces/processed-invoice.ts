import { InvoiceDetail } from './invoice-detail';

export interface ProcessedInvoice {
	id: number;
	approvedBy: string;
	generatedBy: string;
	fullname: string; //fullname of generateBy
	invoiceDetails: InvoiceDetail[];
	createDate: string;
	approvedDate: string;
	rut: string;
	identificationType: string;
	dni: string;
	phone: string;
	email: string;
	bankAccountType: string;
	bankName: string;
	bankAccount: string;
	sign: string;
}
