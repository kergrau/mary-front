import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
	constructor(private readonly tokenService: TokenService) {}

	intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
		let isgoPinata: boolean = this.goToPinata(request);
		if (!isgoPinata && this.tokenService.hasToken()) {
			request = request.clone({
				setHeaders: {
					Authorization: `Bearer ${this.tokenService.getToken()}`
				}
			});
		}
		return next.handle(request);
	}

	goToPinata(request: HttpRequest<unknown>): boolean {
		return request.url.split('/')[2] == 'api.pinata.cloud' ? true : false;
	}
}
