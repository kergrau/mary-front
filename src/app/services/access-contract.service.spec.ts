import { TestBed } from '@angular/core/testing';

import { AccessContractService } from './access-contract.service';

describe('AccessContractService', () => {
  let service: AccessContractService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccessContractService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
