import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UpJsonPinataRequest } from '../interfaces/up-json-pinata-request';

@Injectable({
	providedIn: 'root'
})
export class UploadJsonService {
	constructor(private http: HttpClient) {}

	uploadJsonPinata(data: UpJsonPinataRequest): Observable<any> {
		return this.http.post(`${environment.pinataURI}/pinning/pinJSONToIPFS`, data, {
			headers: { Authorization: environment.pinataJWT }
		});
	}
}
