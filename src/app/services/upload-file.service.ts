import { Injectable } from '@angular/core';
import { MeasurementUnits } from '../enums/measurement-units';
import { UploadFile } from '../enums/upload-file';
import { FileTypes } from '../enums/file-types';
import { HttpClient } from '@angular/common/http';
import { Observable, lastValueFrom } from 'rxjs';
import { UploadedFile } from '../models/uploaded-file';
import { environment } from 'src/environments/environment';
import { UpJsonPinataRequest } from '../interfaces/up-json-pinata-request';

@Injectable({
	providedIn: 'root'
})
export class UploadFileService {
	constructor(private http: HttpClient) {}

	/**
	 * This method is to upload file to Pinata
	 * @param file File to upload to Pinata.
	 * @param keyValueMetadata Object with key value for metadata
	 */
	async upFilePinata(file: File, keyValueMetadata?: Object) {
		let data: FormData = this.prepareToDataUp(file, keyValueMetadata);
		let hash = await lastValueFrom(this.uploadFilePinata(data))
			.then((response: UploadedFile) => {
				console.log(response);
				return response.IpfsHash;
			})
			.catch((err: any) => {
				console.error(err);
				return '';
			});
		return hash;
	}

	uploadFilePinata(data: FormData): Observable<any> {
		return this.http.post(`${environment.pinataURI}/pinning/pinFileToIPFS`, data, {
			headers: { Authorization: environment.pinataJWT }
		});
	}

	validateFileSize(file: File): boolean {
		let size: number = file.size / MeasurementUnits.MB; // Size to MB
		if (size <= UploadFile.LIMIT_SIZE) {
			return true;
		}
		return false;
	}

	validateFileType(file: File): boolean {
		let type: string = file.type.split('/')[1];
		let isAllowed: boolean = FileTypes.ALLOWED.includes(type);
		return isAllowed;
	}

	/**
	 * Prepared data to upload to Pinata
	 * @param file File to upload to Pinata
	 * @param keyValueMetadata Object with file metadata
	 * @returns FormData object
	 */
	prepareToDataUp(file: File, keyValueMetadata?: Object): FormData {
		let data: FormData = new FormData();
		let metadata: Object = { keyvalues: keyValueMetadata };
		let pinMetedataStr: string = JSON.stringify(metadata);
		data.append('file', file);
		if (keyValueMetadata) {
			data.append('pinataMetadata', pinMetedataStr);
		}
		return data;
	}
}
