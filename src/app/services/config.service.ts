import { Injectable } from '@angular/core';
import { Web3 } from 'web3';
import { ABI } from 'src/assets/abi';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class ConfigService {
	private addressContract: string = environment.addressContract;
	private provider: string = environment.provider;
	web3: Web3 = new Web3(new Web3.providers.HttpProvider(this.provider));
	contract: any = new this.web3.eth.Contract(ABI.abi, this.addressContract);
}
