import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { InvoiceDetail } from '../interfaces/invoice-detail';
import { Observable } from 'rxjs';
import { InvoiceToApproval } from 'src/app/interfaces/invoice-to-approval';
import { ProcessedInvoice } from '../interfaces/processed-invoice';

@Injectable({
	providedIn: 'root'
})
export class InvoiceService {
	invoiceRoute: string = `${environment.urlBase}/api/invoice`;
	constructor(private http: HttpClient) {}

	listPendingInvoices(): Observable<InvoiceToApproval[]> {
		return this.http.get<InvoiceToApproval[]>(`${this.invoiceRoute}/list-pending`);
	}

	listProcessed(): Observable<ProcessedInvoice[]> {
		return this.http.get<ProcessedInvoice[]>(`${this.invoiceRoute}/list-processed`);
	}

	listProcessedByUser(): Observable<ProcessedInvoice[]> {
		return this.http.get<ProcessedInvoice[]>(`${this.invoiceRoute}/list-processed-by-user`);
	}

	save(invoice: InvoiceDetail[]): Observable<any> {
		return this.http.post(`${this.invoiceRoute}/save`, invoice);
	}

	approve(id: number, decision: boolean): Observable<any> {
		const params = new HttpParams().set('id', id).set('decision', decision);
		return this.http.patch(`${this.invoiceRoute}/approve`, {}, { params: params });
	}

	/*async getSign(url: string) {
		const res = await fetch(url);
   	const blob = await res.blob();
		console.log("BLOB",blob);

		return new Promise((resolve, reject) => {
			var reader  = new FileReader();
			reader.addEventListener("load", function () {
					resolve(reader.result);
			}, false);
	
			reader.onerror = () => {
				return reject(this);
			};
			reader.readAsDataURL(blob);
		})
  }*/
}
