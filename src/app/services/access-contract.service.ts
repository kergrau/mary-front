import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { UploadFileService } from './upload-file.service';
import { Transaction } from '../models/transaction';
import { environment } from 'src/environments/environment';
import { UploadJsonService } from './upload-json.service';
import { lastValueFrom } from 'rxjs';
import { UpJsonPinataRequest } from '../interfaces/up-json-pinata-request';
import { UploadedFile } from '../models/uploaded-file';

@Injectable({
	providedIn: 'root'
})
export class AccessContractService {
	baseURI: string = 'https://ipfs.io/ipfs/';
	file: File = new File([], ''); // Variable to store file
	constructor(
		private readonly configService: ConfigService,
		private readonly upJson: UploadJsonService
	) {}

	getBalance(address: string) {
		this.configService.contract.methods
			.balanceOf(address)
			.call({ from: address })
			.then((response: number) => {
				console.log(Number(response));
			})
			.catch((err: any) => {
				console.error(err);
			});
	}

	getSymbol(address: string) {
		this.configService.contract.methods
			.symbol(address)
			.call({ from: address })
			.then((response: any) => {
				console.log(response);
			})
			.catch((err: any) => {
				console.error(err);
			});
	}

	/**
	 * Crear un NFT a partir de un archivo siempre y cuando sea menos de
	 * 4MB y sea PNG JPG JPEG o PDF
	 * @param keyValueMetadata Un objeto con los metadatos que se le quieren poner al archivo
	 */
	async mintNFT(keyValueMetadata: UpJsonPinataRequest) {
		let address: string = environment.address;
		let hash: string = '';
		await lastValueFrom(this.upJson.uploadJsonPinata(keyValueMetadata))
			.then((result: UploadedFile) => {
				hash = result.IpfsHash;
				console.log(hash);
			})
			.catch((error) => {
				console.error(error);
			});

		let transaction: Transaction = await this.prepareTran(
			address,
			this.configService.contract.methods.mint(address, hash)
		);

		let signedTransaction = await this.signTransaction(transaction);
		return await this.sendTransaction(signedTransaction);
	}

	/**
	 * Modela el objeto de una transaccion
	 * @param address direccion que va a ejecutar la transaccion
	 * @param method Metodo del contrato que se quiere ejecutar o tranzar
	 * @returns retorna el objeto formado de la transaccion
	 */
	async prepareTran(address: string, method: any) {
		const nonce: bigint = await this.configService.web3.eth.getTransactionCount(
			address,
			'latest'
		);
		const gasEsitmated: number = await method.estimateGas();
		const gasPrice = await this.configService.web3.eth.getGasPrice();
		const transaction: any = {
			from: address,
			to: environment.addressContract, // Contrato que al que esta ligado la libreria
			nonce: Number(nonce),
			gas: gasEsitmated,
			gasPrice: gasPrice,
			data: method.encodeABI()
		};

		console.log(transaction);
		return transaction;
	}

	/**
	 * Firma la transaccion con la llave privada de la cuenta suministrada
	 * se debe configurar la llave privada de la cuenta a utilizar en el
	 * metodo de configuracion antes de ejecutar cualquier transaccion
	 * @param transaction Objeto de la transaccion que se va a firmar
	 * @returns Retorna una promesa con la transaccion firmada
	 */
	async signTransaction(transaction: Transaction) {
		const signPromise = await this.configService.web3.eth.accounts.signTransaction(
			transaction,
			environment.privateKey
		);
		return signPromise;
	}

	/**
	 * Envia la transaccion firmada a la Blockchain
	 * @param signedTransaction Transaccion firmada
	 * @returns Retorna el has de la transaccion o NFT
	 */
	async sendTransaction(signedTransaction: any) {
		let hashTransaction: string = '';
		await this.configService.web3.eth
			.sendSignedTransaction(signedTransaction.rawTransaction)
			.then((response: any) => {
				console.log('Completed transaction');
				console.log(response);
				hashTransaction = response.transactionHash;
			})
			.catch((err: any) => {
				console.error(err);
			});

		console.log(hashTransaction);
		return hashTransaction;
	}
}
