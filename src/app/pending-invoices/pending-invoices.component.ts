import { Component } from '@angular/core';
import { InvoiceService } from 'src/app/services/invoice.service';
import { InvoiceToApproval } from 'src/app/interfaces/invoice-to-approval';
import { AccessContractService } from 'src/app/services/access-contract.service';
import { MetadataNFT } from '../interfaces/metadata-nft';
import { JsonMetadataPinata } from '../interfaces/json-metadata-pinata';
import { UpJsonPinataRequest } from '../interfaces/up-json-pinata-request';
import { TokenService } from '../services/token.service';
import { MessageService, ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-pending-invoices',
  templateUrl: './pending-invoices.component.html',
  styleUrls: ['./pending-invoices.component.scss'],
  providers: [MessageService, ConfirmationService]
})
export class PendingInvoicesComponent {
  listInvoiceToApprove: InvoiceToApproval[] = [];

  visible: boolean = false;
  invoiceSelected!: InvoiceToApproval;

	constructor(
		private invoiceService: InvoiceService,
		private accessContract: AccessContractService,
		private readonly tokenService: TokenService,
		private messageService: MessageService,
    private confirmationService: ConfirmationService
	) {}

	ngOnInit(): void {
		this.getPendingInvoices();
	}

	getPendingInvoices() {
		this.invoiceService.listPendingInvoices().subscribe({
			next: (result: InvoiceToApproval[]) => {
				this.listInvoiceToApprove = result;
			},
			error: (err) => console.error('Error listPendingInvoices ->', err)
		});
	}

  showInvoiceDetail(invoice: InvoiceToApproval) {
    this.visible = true;
    this.invoiceSelected = invoice;
  }

  confirm(invoice: InvoiceToApproval, approved: boolean) {
    this.confirmationService.confirm({
      message: approved ? 'Do you want to approve this invoice?' : 'Do you want to reject this invoice?' ,
      header: approved ? 'Approve action' : 'Reject action',
      rejectLabel: 'Cancel',
      rejectIcon: ' ',
      rejectButtonStyleClass: 'p-button-sm p-button-outlined',
      acceptLabel: approved ? 'Approve': 'Reject',
      acceptIcon: ' ',
      acceptButtonStyleClass: approved ? 'p-button-sm p-button-success' : 'p-button-sm p-button-danger',
      accept: () => {
        this.approvedInvoice(invoice, approved);
      }
    });
  }

	approvedInvoice(invoice: InvoiceToApproval, approved: boolean) {
		// Se crea objeto con los metadatos deseados
		let prepareMetadata: MetadataNFT = {
			rut: invoice.rut,
			date: invoice.createDate,
			approvedBy: this.tokenService.getUsername(),
			invoice: invoice.invoiceDetails
		};
		let prepareMetadataJson: JsonMetadataPinata = {
			name: `${invoice.generatedBy}_${invoice.id}`
		};
		let pinataMetadata: UpJsonPinataRequest = {
			pinataContent: prepareMetadata,
			pinataMetadata: prepareMetadataJson
		};

		this.invoiceService.approve(invoice.id, approved).subscribe({
			next: () => {
				if (approved) {
					this.accessContract.mintNFT(pinataMetadata).then(() => {
						this.messageService.add({
							severity: 'success',
							summary: 'Processed Invoice',
							detail: 'Invoice was approved'
						});
					});
				} else {
					this.messageService.add({
						severity: 'info',
						summary: 'Processed Invoice',
						detail: 'Invoice was rejected'
					});
				}
				this.getPendingInvoices();
			},
			error: (error) => {
				console.log(error);
			}
		});
	}
}
