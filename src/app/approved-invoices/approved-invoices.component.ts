import { Component, OnInit } from '@angular/core';
import { InvoiceService } from 'src/app/services/invoice.service';
import { TokenService } from 'src/app/services/token.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { Role } from 'src/app/enums/role';
import { ProcessedInvoice } from 'src/app/interfaces/processed-invoice';
import jsPDF from 'jspdf';
import { UtilService } from 'src/app/services/util.service';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-approved-invoices',
	templateUrl: './approved-invoices.component.html',
	styleUrls: ['./approved-invoices.component.scss'],
	providers: [MessageService, ConfirmationService]
})
export class ApprovedInvoicesComponent implements OnInit {
	listApprovedInvoices: ProcessedInvoice[] = [];

	constructor(
		private invoiceService: InvoiceService,
		private tokenService: TokenService,
		private confirmationService: ConfirmationService,
		private utilService: UtilService
	) {}

	ngOnInit(): void {
		this.getListApprovedInvoices();
	}

	getListApprovedInvoices() {
		this.getListApprovedInvoicesByRole().subscribe({
			next: (result: ProcessedInvoice[]) => {
				this.listApprovedInvoices = result;
			},
			error: (err) => console.error('Error listApprovedInvoices -> ', err)
		});
	}

	getListApprovedInvoicesByRole() {
		if (this.tokenService.hasRole(Role.ADMIN)) return this.invoiceService.listProcessed();

		return this.invoiceService.listProcessedByUser();
	}

	confirm(invoice: ProcessedInvoice) {
		this.confirmationService.confirm({
			message: `Do you want to print the report for invoice ${invoice.id} ?`,
			header: 'Print Report',
			rejectLabel: 'Cancel',
			rejectIcon: ' ',
			rejectButtonStyleClass: 'p-button-sm p-button-outlined',
			acceptLabel: 'Print',
			acceptIcon: ' ',
			acceptButtonStyleClass: 'p-button-sm p-button-primary',
			accept: () => {
				this.printPDF(invoice);
			}
		});
	}

	printPDF(invoice: ProcessedInvoice) {
		/*let img = "";
    this.invoiceService.getSign("https://images.pexels.com/lib/api/pexels-white.png")
    .then(result => console.log("RESULT", result))
    .catch(err => console.error("Error ->",err))*/

		let listItems: string[] = [];
		let amount: number = 0;

		for (let detail of invoice.invoiceDetails) {
			listItems.push(detail.item);
			amount += Number(detail.price);
		}

		const fontSizeTitle = 12;
		const fontSizeText = 10;
		const x = 10;
		let y = 10;
		const h = 10;
		const doc = new jsPDF({ orientation: 'portrait', format: 'a4' });
		doc.setFontSize(fontSizeText);
		doc.setTextColor('#616161');
		doc.text('Cuenta de cobro 001', 200, y, { align: 'right' });
		y += h;

		doc.setFontSize(fontSizeTitle);
		doc.setTextColor('#000000');
		let date = new Date();
		//doc.text(`Fecha: ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`, x, y);
		doc.text(`Fecha: ${invoice.approvedDate}`, x, y);
		y += h;
		y += h;

		doc.setFont('helvetica', 'normal', 'bold');
		doc.text('NOMBRE DE LA EMPRESA A COBRAR', 100, y, { align: 'center' });
		y += h;

		doc.text('NIT: 888.000.999.-7', 100, y, { align: 'center' });
		y += h;
		y += h;

		doc.setFont('helvetica', 'normal', 'normal');
		doc.text('El siguiente monto es adeudado a:', 100, y, { align: 'center' });
		y += h;

		doc.text(invoice.fullname, 100, y, { align: 'center' });
		y += h;

		doc.text(invoice.identificationType + ' ' + invoice.dni, 100, y, { align: 'center' });
		y += h;
		y += h;

		doc.text('Por la suma de:', 100, y, { align: 'center' });
		y += h;

		doc.setFont('helvetica', 'normal', 'bold');
		doc.text(
			`$${amount.toLocaleString('es-CO')} (${this.utilService.capitalizeFirtsLetter(
				this.utilService.numeroALetras(amount)
			)})`,
			100,
			y,
			{ align: 'center' }
		);
		y += h;
		y += h;

		doc.setFont('helvetica', 'normal', 'normal');
		doc.text('Por concepto de:', 100, y, { align: 'center' });
		y += h;

		doc.text(listItems, 100, y, { align: 'center' });
		y += h;
		y += h;

		doc.text(
			'Por favor realizar el pago a la cuenta de ' +
				invoice.bankAccountType +
				' No. ' +
				invoice.bankAccount +
				' del Banco ' +
				invoice.bankName +
				'. Cualquier inquietud sobre esta cuenta, favor comunicarce al celular ' +
				invoice.phone +
				' o al correo ' +
				invoice.email +
				'.',
			x,
			y,
			{ maxWidth: 180, align: 'justify' }
		);
		y += h;
		y += h;
		y += h;
		y += h;

		doc.text('Cordialmente,', x, y);
		y += h;
		doc.addImage(`${environment.domainPinata}${invoice.sign}`, 'PNG', x, y, 50, 10, 'sing');
		y += h;

		doc.text('________________________', x, y);
		y += h - 5;

		doc.text(invoice.fullname, x, y);
		y += h - 5;

		doc.text(invoice.identificationType + ': ' + invoice.dni, x, y);
		y += h - 5;

		doc.text('Celular: ' + invoice.phone, x, y);
		doc.save(`Cuenta_de_cobro_${invoice.dni}_${date.getMilliseconds()}.pdf`);
	}
}
