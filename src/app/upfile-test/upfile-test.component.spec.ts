import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpfileTestComponent } from './upfile-test.component';

describe('UpfileTestComponent', () => {
  let component: UpfileTestComponent;
  let fixture: ComponentFixture<UpfileTestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpfileTestComponent]
    });
    fixture = TestBed.createComponent(UpfileTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
