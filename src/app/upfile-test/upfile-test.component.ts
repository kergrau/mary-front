import { Component, OnInit } from '@angular/core';
import { UploadFileService } from '../services/upload-file.service';
import { environment } from 'src/environments/environment';
import { AccessContractService } from '../services/access-contract.service';
import { MetadataNFT } from '../interfaces/metadata-nft';
import { UpJsonPinataRequest } from '../interfaces/up-json-pinata-request';
import { InvoiceService } from '../services/invoice.service';
import { InvoiceToApproval } from '../interfaces/invoice-to-approval';
import { JsonMetadataPinata } from '../interfaces/json-metadata-pinata';
import { TokenService } from '../services/token.service';

@Component({
	selector: 'app-upfile-test',
	templateUrl: './upfile-test.component.html',
	styleUrls: ['./upfile-test.component.scss']
})
export class UpfileTestComponent implements OnInit {
	file: File = new File([], ''); // Variable to store file
	declare invoiceToApprove: InvoiceToApproval;
	constructor(
		private readonly upFileService: UploadFileService,
		private readonly accessContract: AccessContractService,
		private readonly invoiceService: InvoiceService,
		private readonly tokenService: TokenService
	) {}

	ngOnInit(): void {
		this.accessContract.getBalance(environment.address);
		this.listPendingInvoices();
	}

	listPendingInvoices() {
		this.invoiceService.listPendingInvoices().subscribe({
			next: (response: InvoiceToApproval[]) => {
				this.invoiceToApprove = response[0];
			},
			error: (error: any) => {
				console.log(error);
			}
		});
	}

	async pruebaSubida(invoiceToApprove: InvoiceToApproval, decision: boolean) {
		// Se crea objeto con los metadatos deseados
		let prepareMetadata: MetadataNFT = {
			rut: invoiceToApprove.rut,
			date: invoiceToApprove.createDate,
			approvedBy: this.tokenService.getUsername(),
			invoice: invoiceToApprove.invoiceDetails
		};
		let prepareMetadataJson: JsonMetadataPinata = {
			name: `${invoiceToApprove.generatedBy}_${invoiceToApprove.id}`
		};
		let pinataMetadata: UpJsonPinataRequest = {
			pinataContent: prepareMetadata,
			pinataMetadata: prepareMetadataJson
		};

		this.invoiceService.approve(invoiceToApprove.id, decision).subscribe({
			next: () => {
				if (decision) {
					this.accessContract.mintNFT(pinataMetadata);
				}
			},
			error: (error) => {
				console.log(error);
			}
		});
	}
}
