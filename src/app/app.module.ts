import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { GenerateInvoicesComponent } from './generate-invoices/generate-invoices.component';
import { PendingInvoicesComponent } from './pending-invoices/pending-invoices.component';
import { ApprovedInvoicesComponent } from './approved-invoices/approved-invoices.component';

import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { DropdownModule } from 'primeng/dropdown';
import { FileUploadModule } from 'primeng/fileupload';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { TooltipModule } from 'primeng/tooltip';
import { InputNumberModule } from 'primeng/inputnumber';
import { TableModule } from 'primeng/table';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { MenuComponent } from './menu/menu.component';

@NgModule({
    declarations: [AppComponent, LoginComponent, SignUpComponent, DashboardComponent, HomeComponent, GenerateInvoicesComponent, PendingInvoicesComponent, MenuComponent, ApprovedInvoicesComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        CardModule,
        ButtonModule,
        ReactiveFormsModule,
        FormsModule,
        InputTextModule,
        PasswordModule,
        DropdownModule,
        FileUploadModule,
        ToastModule,
        MessagesModule,
        TooltipModule,
        InputNumberModule,
        TableModule,
        ConfirmDialogModule,
        DialogModule
    ],
    providers: [
			{
				provide: HTTP_INTERCEPTORS,
				useClass: TokenInterceptor,
				multi: true
			}
		],
    bootstrap: [AppComponent],
})
export class AppModule {}
